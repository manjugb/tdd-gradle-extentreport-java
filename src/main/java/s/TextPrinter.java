package s;

import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TextPrinter {
    TextManipulator textManipulator;
    protected final Logger logger = LogManager.getLogger(getClass());

    public TextPrinter(TextManipulator textManipulator) {
        this.textManipulator = textManipulator;
    }

    public void printText() {
        logger.info(textManipulator.getText());
    }

    public void printOutEachWordOfText() {
        logger.info(Arrays.toString(textManipulator.getText().split(" ")));
    }

    public void printRangeOfCharacters(int startingIndex, int endIndex) {
        logger.info(textManipulator.getText().substring(startingIndex, endIndex));
    }
}