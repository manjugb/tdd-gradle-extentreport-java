package javalearning;

public class SingletonFactory {
	private SingletonFactory(){}
	
	private static class AHolder {
		private static final  SingletonFactory INSTANCE = new SingletonFactory();
		
	}
	public static SingletonFactory getInstance() {
		System.out.println("Singleton Factory");
		return AHolder.INSTANCE;
	}
	
	public  static void main(String args[]) {
		getInstance();
	}

}
