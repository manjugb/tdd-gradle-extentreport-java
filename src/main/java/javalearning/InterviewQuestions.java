package javalearning;

public class InterviewQuestions {
/**
 * find the input and output of the program
 * 
 */
	 
		public static boolean isFoo(String param) {
			if(param == "foo") {
				//System.out.println(isFoo(param));
			return true;
			
		}else {
			
			return false;
		}
	}
		
	public static int  isDivideValue(int a,int b) {
		
		return a / b;
	}
	
	public int findLargestNumber(int[] numbers) {
		int size = numbers.length;
		int max = numbers[0];
		for(int i=0;i<size;i++) {
			if(max < numbers[i]) {
				max =  numbers[i];
			}
		}
		
		return max;
		
	}
	
	public int findSecondLargestNumberInArray(int[] arr) {
		int firstlargest = 0;
		int secondlargest = 0;
		int i = 0;
		int n = arr.length;
		for (i = 0; i < n; i++) {

			if (arr[i] > firstlargest) {
				secondlargest = firstlargest;
				firstlargest = arr[i];
			}
		}

		//logger.info(firstlargest + "  " + secondlargest);

		return secondlargest;

	}
	
	public int FindMaximumNumberInArray(int[] numbers) {

		/**
		 * {@summary} this method describes the find maximum number in an array
		 */
		int maxValue = numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] > maxValue) {
				maxValue = numbers[i];
			}
		}
		return maxValue;

	}

	
	 		
	
}
