package javalearning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

public class CollectionClass {

    //private test ts;
	public static void main(String args[]) throws Throwable {
		checkHashMap();
		treeMap();
	
		
	}
	
	public static int ArrayListSize(int item) throws Throwable{
		ArrayList<Integer> l = new ArrayList(2);
		l.add(item);
		l.add(item);
		l.add(item);
		System.out.println(l);
		return l.size();
		
	}
	
	public static int HashSetSize() throws Throwable{
		HashSet s = new HashSet();
		s.add(new Integer(0));
		s.add(new Integer(0));
		s.add(new Integer(0));
		System.out.println(s.size());
		return s.size();
		
	}
	
	public static void checkHashMap() {
		HashMap m = new HashMap();
	    Object o1 = new Object();
	    Object o2 = o1;
	    m.put(o1,1);
	    m.put(o2, 2);
	    System.out.println(m.get(o2));
	    System.out.println(m.get(o1));
	}
	
	
	public static void treeMap() {
		TreeMap t = new TreeMap();
		t.put(3, 3);
		t.put(2, 1);
		t.put(1, 2);
		System.out.println(t.values());
		
		
	}

	
	

}
