package javalearning;

public class Light {

	protected static final long hh = 24;
	protected static final long ss = 60;
	protected static final long mm = 60;

	public long LightDistance(long lightspeed, long days) {
		long seconds, distance;
		seconds = days * hh * ss * mm;// converted to seconds
		distance = lightspeed * seconds; // light speed in seconds
		System.out.println("In" + days);
		System.out.println("days light will travel about ");
		System.out.println(distance + "miles");
		return distance;

	}

}
