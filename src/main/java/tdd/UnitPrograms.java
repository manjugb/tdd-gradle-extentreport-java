package tdd;

import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * @author manjunath {@summary} this class defines coding challenges faced in
 *         interviews
 */
public class UnitPrograms {
	/**
	 * 
	 * @param arr
	 * @return secondlargest
	 */
	 protected final Logger logger = LogManager.getLogger(getClass());

	public int FindSecondLargestNumberInArray(int[] arr) {
		int firstlargest = 0;
		int secondlargest = 0;
		int i = 0;
		int n = arr.length;
		for (i = 0; i < n; i++) {

			if (arr[i] > firstlargest) {
				secondlargest = firstlargest;
				firstlargest = arr[i];
			}
		}

		logger.info(firstlargest + "  " + secondlargest);

		return secondlargest;

	}

	public int FindMaximumNumberInArray(int[] numbers) {

		/**
		 * {@summary} this method describes the find maximum number in an array
		 */
		int maxValue = numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] > maxValue) {
				maxValue = numbers[i];
			}
		}
		return maxValue;

	}

	public int FindMinimumNumberInArray(int[] numbers) {

		/**
		 * {@summary} this method describes the find maximum number in an array
		 */
		int minValue = numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] < minValue) {
				minValue = numbers[i];
			}
		}
		return minValue;

	}

//Find minimum (lowest) value in array using array sort
	public static int MinValueUsingArraySort(int[] numbers) {
		Arrays.sort(numbers);
		return numbers[0];
	}

//Find maximum (largest) value in array using array sort
	public static int MaxValue_UsingArraySort(int[] numbers) {
		Arrays.sort(numbers);
		return numbers[numbers.length - 1];
	}
	
	

}
