package tdd;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author manjunath {@summary} this class defines coding challenges faced in
 *         interviews
 */
public class unitprograms {
	/**
	 * 
	 * @param arr
	 * @return secondlargest
	 */

	public int findSecondLargestNumberInArray(int[] arr) {
		int firstlargest = 0;
		int secondlargest = 0;
		int i = 0;
		int n = arr.length;
		for (i = 0; i < n; i++) {

			if (arr[i] > firstlargest) {
				secondlargest = firstlargest;
				firstlargest = arr[i];
			}
		}

		System.out.println(firstlargest + "  " + secondlargest);

		return secondlargest;

	}

	public int findMaximumNumberInArray(int[] numbers) {

		/**
		 * {@summary} this method describes the find maximum number in an array
		 */
		int maxValue = numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] > maxValue) {
				maxValue = numbers[i];
			}
		}
		return maxValue;

	}

	public int findMinimumNumberInArray(int[] numbers) {

		/**
		 * {@summary} this method describes the find maximum number in an array
		 */
		int minValue = numbers[0];
		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] < minValue) {
				minValue = numbers[i];
			}
		}
		return minValue;

	}

//Find minimum (lowest) value in array using array sort
	public static int minValueUsingArraySort(int[] numbers) {
		Arrays.sort(numbers);
		return numbers[0];
	}

//Find maximum (largest) value in array using array sort
	public static int maxValue_UsingArraySort(int[] numbers) {
		Arrays.sort(numbers);
		return numbers[numbers.length - 1];
	}
	
	

}
