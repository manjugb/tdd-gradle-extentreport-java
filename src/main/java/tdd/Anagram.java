package tdd;

import java.util.HashSet;
import java.util.Set;

public class Anagram {

	//Anagrams using hashset
		public boolean isAnagrams1(String x, String y) {
		    if (x.length() != y.length()) {
		        return false;
		    } else if (x.equals(y)) {
		        return true;
		    }

		    Set<Character> anagramSet = new HashSet<>();
		    for (int i = 0; i < x.length(); i++) {
		        anagramSet.add(x.charAt(i));
		        anagramSet.add(y.charAt(i));
		    }

		    return anagramSet.size() != x.length();
		}
		
		//using character arrays
		public boolean checkAnagram(String stringOne , String stringTwo){
	        char[] first = stringOne.toLowerCase().toCharArray(); 
	        char[] second = stringTwo.toLowerCase().toCharArray();
	        // if length of strings is not same 
	        if (first.length != second.length)
	            return false;
	        int[] counts = new int[26]; 
	        for (int i = 0; i < first.length; i++){
	            counts[first[i]-97]++;  
	            counts[second[i]-97]--;   
	        }
	        for (int i = 0; i<26; i++)
	            if (counts[i] != 0)
	                return false;
	        return true;
	    }
		
		public static boolean isAnagram2(String str1, String str2) {
		    if (str1.length() != str2.length()) {
		        return false;
		    }

		    for (int i = 0; i < str1.length(); i++) {
		        char ch = str1.charAt(i);

		        if (str2.indexOf(ch) == -1) 
		            return false;
		        else
		            str2 = str2.replaceFirst(String.valueOf(ch), " ");
		    }

		    return true;
		}
	
}
