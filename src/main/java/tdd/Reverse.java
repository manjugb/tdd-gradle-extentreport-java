package tdd;



public class Reverse {
	//find reverse of a string
	
/*	public static void main(String args[]) {
		reverse("test");
		Reverse rev = new Reverse();
		rev.removeSpecialCharInString("dfd#$#3");
		
	}*/


	
		public static String reverse(String s) {
			String rev = "";
			if (s != null && !s.isEmpty()) {
				int len = s.length();
				for (int i = len - 1; i >= 0; i--) {
					rev = rev + s.charAt(i);
				}
			}
			System.out.println("Reverse" + rev + "String");
			return rev;

		}
		
		
		public StringBuffer reverseWithStringBuffer(String s) {
			 StringBuffer sf = new StringBuffer(s);
			 StringBuffer s1 = sf.reverse();
			

			return s1;
		}

		// Write a function remove special characters in string

		public String removeSpecialCharInString(String s) {

			if (s != null && !s.isEmpty()) {
				s = s.replaceAll("[^a-zA-Z0-9]", "");
				System.out.println("After Remove Special Charaters:" + s + "String");
			}

			return s;

		}
		
		public int checkReverseOfNumber(int num) {
			int rev = 0;
			while (num != 0) {
				rev = rev * 10 + num % 10;
				num = num / 10;
			}
			System.out.println("Reverse of a Number:" + rev);
			return rev;
		}
		
		public StringBuffer checkReverseOfNumberWithStringBuffer(long num) {
			StringBuffer revnum;
			revnum = new StringBuffer(String.valueOf(num)).reverse();
			return revnum;

		}
}
