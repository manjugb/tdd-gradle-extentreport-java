package tdd;

import java.util.ArrayList;

public class MissingNumber {

	public long missingNumber(long[] arr) {
		long sum = 0;
		long sum1 = 0;
		long mnum;
		int arraySize;
		arraySize = arr.length;
		// sum of arry without missing number
		for (int i = 0; i < arraySize; i++) {
			sum = sum + arr[i];
		}
		System.out.println("Sum with out missing Number:" + sum);
		for (int j = 0; j <= arraySize + 1; j++) {
			sum1 = sum1 + j;
			System.out.println(sum1);
		}
		System.out.println("Sum with missing Number:" + sum1);
		mnum = sum1 - sum;
		return mnum;

	}

	// calculate missing number in n number o(2n)

	public static int findMissingN(int[] arr) {
		int sum = 0, max = 0;
		int mnum = 0;
		for (int n : arr) {
			sum += n;
			if (n > max)
				max = n;
		}
		mnum = (max * (max + 1) / 2) - sum;
		System.out.println("Missing Number:" + mnum);
		return mnum;
	}

	// find missing numbers

	public static ArrayList<Integer> findMissing4(ArrayList<Integer> arr) {
		int max = 0;
		ArrayList<Integer> misNums = new ArrayList();
		int[] neededNums;
		for (int n : arr) {
			if (n > max)
				max = n;
		}
		neededNums = new int[max];// zero for any needed num
		for (int n : arr) {// iterate again
			neededNums[n == max ? 0 : n]++;// add one - used as index in second array (convert max to zero)
		}
		for (int i = neededNums.length - 1; i > 0; i--) {
			if (neededNums[i] < 1)
				misNums.add(i);// if value is zero, than index is a missing number
		}
		return misNums;
	}
}
