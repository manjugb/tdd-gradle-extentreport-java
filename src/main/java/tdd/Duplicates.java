package tdd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Duplicates {
	// find duplicates in an array worst case o(nxn)

	public static String findDuplicates(String names[]) {
		int i = 0, j;
		if (names != null && names.length != 0) {
			for (i = 0; i < names.length; i++) {
				for (j = i + 1; j < names.length; j++) {
					if (names[i].equals(names[j])) {
						return names[i];

					}System.out.println("Duplicate Word:" + names[i]);
				}
			}
			
			
		} else {
			System.out.println("There were no duplicates");
		}
		return names[i];

	}

	// duplicates o(n) using hashSets

	public static String findDuplicatesHashSets(String[] names) {
		Set<String> store = new HashSet<String>();
		for (String name : names) {
			if (store.add(name) == false) {
				return name;
			}
			System.out.println("Found duplicate word:" + name);
		}
		return "\0";

	}

	// o(2n) time complexity

	public static String findDuplicatesHashMap(String[] names) {
		String sword = "";
		// Store values into hashMap
		Map<String, Integer> storeMap = new HashMap<String, Integer>();

		for (String name : names) {
			Integer count = storeMap.get(name);
			if (count == null) {
				storeMap.put(name, 1);
			} else {
				storeMap.put(name, ++count);
			}
		}

		// get the values hashmap objects entry set and gives key and value
		Set<Entry<String, Integer>> entrySet = storeMap.entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			if (entry.getValue() > 1) {
				System.out.println("duplicate element:" + entry.getKey() + "Value:" + entry.getValue());
				sword = entry.getKey();

				return sword;
			}
			// return entry.getKey();

		}
		return sword;

	}
}
