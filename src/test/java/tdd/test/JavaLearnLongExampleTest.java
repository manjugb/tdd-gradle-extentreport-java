package tdd.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import javalearning.Light;
import tdd.Anagram;

public class JavaLearnLongExampleTest extends TestBaseClass{
public Light agm = new Light();
    
	@Test(groups="Light")
	public void LightSpeedTest_positive() throws Throwable{
		test = extent.createTest("Light Distance", "Calculate Light Distance")
                .assignCategory("unit_TestCase_light")
                .assignCategory("Postive_TestCase_light")
                .assignAuthor("Manjunath");
        logger.info("Light Distance");
        long lightspeed = 500;
        long days = 10;
        long expdistance=432000000;
               
				
         long actdistance = agm.LightDistance(lightspeed, days);
         String strDistance = Long.toString(actdistance);
         if(actdistance == expdistance) {
        	 assertEquals(actdistance, expdistance);
        	 test.generateLog(Status.PASS, strDistance);
        	        	 
         }else {
        	 assertNotEquals(actdistance, expdistance);
         }
	
		
	}
	
	@Test(groups="Light")
	public void LightSpeedTest_Negative() throws Throwable{
		test = extent.createTest("Light Distance", "Calculate Light Distance")
                .assignCategory("unit_TestCase_light")
                .assignCategory("Postive_TestCase_light")
                .assignAuthor("Manjunath");
        logger.info("Light Distance");
        long lightspeed = 50;
        long days = 10;
        long expdistance=432000;
               
				
         long actdistance = agm.LightDistance(lightspeed, days);
         String strDistance = Long.toString(actdistance);
         if(actdistance != expdistance) {
        	 assertNotEquals(actdistance, expdistance);
        	 test.generateLog(Status.PASS, strDistance);
        	        	 
         }else {
        	 assertNotEquals(actdistance, expdistance);
        	 test.generateLog(Status.WARNING, strDistance);
         }
	
		
	}
	

}
