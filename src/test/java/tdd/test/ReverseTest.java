package tdd.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import tdd.Reverse;

public class ReverseTest extends TestBaseClass {

	protected final Logger logger = LogManager.getLogger(getClass());
	public Reverse rev = new Reverse();
	
	@Test(groups="Reverse")
	public void ReverseTest() throws Throwable {
		test = extent.createTest("Reverse of String", "Validate Reverse of String ")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
        
        String StrInput = "Bangalore";
        String expResults =  "erolagnaB";
        String actResults = rev.reverse(StrInput);
        if (actResults.equalsIgnoreCase(expResults)) {
        	assertEquals(actResults, expResults);
        	test.log(Status.PASS, actResults);
        	test.generateLog(Status.PASS, actResults);
        }else {
        	assertNotEquals(actResults, expResults);
        	test.log(Status.FAIL, actResults);
        	test.generateLog(Status.FAIL, actResults);
        	test.generateLog(Status.FAIL, expResults);
        }
	}
	
	
	@Test(groups="Reverse")
	public void ReverseTestStringBuffer() throws Throwable {
		test = extent.createTest("Reverse of String with String Buffer", "Validate Reverse of String using String Buffer ")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
        
        String StrInput = "Bangalore";
        String expResults =  "erolagnaB";
        StringBuffer actResults = rev.reverseWithStringBuffer(StrInput);
        String actResul=actResults.toString();
        if (actResul.equalsIgnoreCase(expResults)) {
        	assertEquals(actResul, expResults);
        	test.log(Status.PASS, actResul);
        	test.generateLog(Status.PASS, actResul);
        }else {
        	assertNotEquals(actResul, expResults);
        	test.log(Status.FAIL, actResul);
        	test.generateLog(Status.FAIL, actResul);
        	test.generateLog(Status.FAIL, expResults);
        }
	}
	
	
	@Test(groups="Reverse")
	public void RemoveSpcialCharacters() throws Throwable {
		test = extent.createTest("Remove Special Characters", "Validate Remove Special Characters ")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
        
        String StrInput = "Bangalore@@#@2";
        String expResults =  "Bangalore2";
        String actResults = rev.removeSpecialCharInString(StrInput);
        //String actResul=actResults.toString();
        if (actResults.equalsIgnoreCase(expResults)) {
        	assertEquals(actResults, expResults);
        	test.log(Status.PASS, actResults);
        	test.generateLog(Status.PASS, actResults);
        }else {
        	assertNotEquals(actResults, expResults);
        	test.log(Status.FAIL, actResults);
        	test.generateLog(Status.FAIL, actResults);
        	test.generateLog(Status.FAIL, expResults);
        }
	}
}
