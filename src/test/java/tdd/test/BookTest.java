package tdd.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import s.Book;


public class BookTest extends TestBaseClass{
	String name;
	String author;
	String text="SreelathManjunath";
	String text2 = "test";
	
	public Book bk = new Book(name,author,text);
	protected final Logger logger = LogManager.getLogger(getClass());
	@Test(groups="untit")
	public void ReplaceWordInTextTest() throws Throwable{
		logger.info("testfindsecondlargest_success started Execution");
		test = extent.createTest("check replace Word in Text", "Word in text")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("word in text");
		String check = bk.replaceWordInText("Bangalore");
		Boolean isText = bk.isWordInText(text);
		if(isText && check.equalsIgnoreCase(text)) {
		Assert.assertEquals(check, text);
		test.log(Status.PASS, check);
		test.generateLog(Status.PASS, check);
		}
		else  {
			
			Assert.assertNotEquals(check, text);
			test.generateLog(Status.FAIL, check);
			test.log(Status.FAIL,check);
		}
	}
}
