package tdd.test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import tdd.UnitPrograms;


public class componentTests extends TestBaseClass {
	
	
	public UnitPrograms cg = new UnitPrograms();
	protected final Logger logger = LogManager.getLogger(getClass());

 
	@Test(groups="unit")
	public void testfindsecondlargest_success() throws Throwable {
		logger.info("testfindsecondlargest_success started Execution");
		test = extent.createTest("Validate Second Largest Number", "Test Second Largest Number")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Validate Second Largest Number");
		int[] arr = { 0, 3, 5, 4, 6, 7 };
		int expsendlargestvalue = 6;
		int actSecondLargest = cg.FindSecondLargestNumberInArray(arr);
		String SecondLargest = Integer.toString(actSecondLargest);
		if(expsendlargestvalue == actSecondLargest) {
			assertEquals(expsendlargestvalue, actSecondLargest);
			logger.info("Second Largest Value:"+actSecondLargest);
			//String SecondLargest = int.toString(actSecondLargest)
			
			 test.log(Status.PASS, SecondLargest);
			}
			else {
				assertNotEquals(expsendlargestvalue, actSecondLargest);
				logger.info("Second Largest Value:"+actSecondLargest);
				test.log(Status.FAIL, SecondLargest);
			}
		logger.info("testfindsecondlargest_success ended Execution");
	}
	
	
	@Test(groups="unit")
	public void testfindsecondlargest_Empty() throws Throwable {
		logger.info("testfindsecondlargest_success started Execution");
		test = extent.createTest("Validate Second Largest Number", "Test Second Largest Number")
                .assignCategory("unit_TestCase")
                .assignCategory("Negative_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Validate Second Largest Number");
		
		logger.info("testfindsecondlargest_success started Execution");
		int[] arr = {};
		int expsendlargestvalue = 0;
		int actSecondLargest = cg.FindSecondLargestNumberInArray(arr);
		String secondlargestValue = Integer.toString(actSecondLargest);
		if(expsendlargestvalue == actSecondLargest) {
			
		assertEquals(expsendlargestvalue, actSecondLargest);
	
		test.log(Status.PASS, secondlargestValue);
		}
		else {
			assertNotEquals(expsendlargestvalue, actSecondLargest);
			test.log(Status.FAIL, secondlargestValue);
		}
		logger.info("testfindsecondlargest_success ended Execution");

	}
	
	@Test(groups="unit")
	public void testfindMaximumNumberInArray() throws Throwable{
		test = extent.createTest("Validate Largest Number", "Validate Maximum Number")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
		
		int[] arr = {20,34,7,23,45,2};
		
		int expmaxNumber = 45;
		int actmaxNumber = cg.FindMaximumNumberInArray(arr);
		String maxNumber = Integer.toString(actmaxNumber);
		if(expmaxNumber == actmaxNumber) {
			assertEquals(expmaxNumber, actmaxNumber);
			test.log(Status.PASS, maxNumber);
		}else {
			assertNotEquals(expmaxNumber, actmaxNumber);
			test.log(Status.FAIL, maxNumber);
		}
	}
	
	
	@Test(groups="unit")
	public void testfindMinimumNumberInArray() throws Throwable{
		test = extent.createTest("Validate Smallest Number", "Validate Minimum Number")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
		
		int[] arr = {20,34,7,23,45,2};
		
		int expminNumber = 2;
		int actminNumber = cg.FindMinimumNumberInArray(arr);
		String minNumber = Integer.toString(actminNumber);
		if(expminNumber == actminNumber) {
			assertEquals(expminNumber, actminNumber);
			test.log(Status.PASS, minNumber);
		}else {
			assertNotEquals(expminNumber, actminNumber);
			test.log(Status.FAIL, minNumber);
		}
	}
	
	
	@Test(groups="unit")
	public void testfindLowestNumberInArray() throws Throwable{
		test = extent.createTest("Validate Smallest Number using Array Sort", "Validate Minimum Number using Array Sort")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
		
		int[] arr = {20,34,71,23,45,2};
		
		int expminNumber = 2;
		int actminNumber = cg.MinValueUsingArraySort(arr);
		String minNumber = Integer.toString(actminNumber);
		if(expminNumber == actminNumber) {
			assertEquals(expminNumber, actminNumber);
			test.log(Status.PASS, minNumber);
		}else {
			assertNotEquals(expminNumber, actminNumber);
			test.log(Status.FAIL, minNumber);
		}
	}
	
	
	@Test(groups="unit")
	public void testfindHighestNumberInArray() throws Throwable{
		test = extent.createTest("Validate Highest Number using Array Sort", "Validate Highest Number using Array Sort")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
		
		int[] arr = {20,34,74,23,45,2};
		
		int expminNumber = 74;
		int actminNumber = cg.MaxValue_UsingArraySort(arr);
		String minNumber = Integer.toString(actminNumber);
		if(expminNumber == actminNumber) {
			assertEquals(expminNumber, actminNumber);
			test.log(Status.PASS, minNumber);
		}else {
			assertNotEquals(expminNumber, actminNumber);
			test.log(Status.FAIL, minNumber);
		}
	}
	
	
	
	
	

	
	

}
