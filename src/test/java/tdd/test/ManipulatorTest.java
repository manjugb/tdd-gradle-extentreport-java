package tdd.test;



import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import junit.framework.Assert;
import s.TextManipulator;

public class ManipulatorTest extends TestBaseClass {
	private String text;
	@Test(groups="Manipulator")
	public void testManiputorGetText() throws Throwable{
		text = "Manjunath";
		TextManipulator texmn = new TextManipulator(text);
		test = extent.createTest("GetText", "GetText")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Get Text");
        String acttext = texmn.getText();
        if(acttext.equalsIgnoreCase(text)) {
        	 Assert.assertEquals(text, acttext);
        	 test.log(Status.PASS, acttext);
        }else
        {
        	 Assert.assertNotSame(text, acttext);
        	 test.log(Status.PASS, acttext);
        }
       
	
}
	
	@Test(groups="Manipulator")
	public void testManiputorAppendText() throws Throwable{
		text = "Manjunath";
		String newText = "Golla Bala";
		String expText = "ManjunathGolla Bala";
		TextManipulator texmn = new TextManipulator(text);
		test = extent.createTest("AppendText", "AppendText")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Append Text");
       texmn.appendText(newText);
       String newtext = texmn.getText();
      // test.generateLog(Status.INFO, newtext);
       if (newtext.equalsIgnoreCase(expText)) {
    	   Assert.assertEquals(expText, newtext);
    	   test.log(Status.PASS, newtext);
    	   test.generateLog(Status.PASS, newtext);
    	   test.generateLog(Status.PASS, expText);
    	   
       }else {
    	   Assert.assertNotSame("Not Matched", expText, newtext);
    	   test.log(Status.FAIL, newtext);
    	   test.generateLog(Status.FAIL, newtext);
    	   test.generateLog(Status.FAIL, expText);
    	   
       }
     }	

	
	@Test(groups="Manipulator")
	public void testFindwordReplace() throws Throwable{
		text = "Manjunath";
	
		String expText = "Welcome to Bangalore";
		
		TextManipulator texmn = new TextManipulator(text);
		test = extent.createTest("FindWordRepalace", "FindWordReplace")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Append Text");
      
       String newtext = texmn.findWordAndReplace(text, expText);
      // test.generateLog(Status.INFO, newtext);
       if (newtext.equalsIgnoreCase(expText)) {
    	   Assert.assertEquals(expText, newtext);
    	   test.log(Status.PASS, newtext);
    	   test.generateLog(Status.PASS, newtext);
    	   test.generateLog(Status.PASS, expText);
    	   
       }else {
    	   Assert.assertNotSame("Not Matched", expText, newtext);
    	   test.log(Status.FAIL, newtext);
    	   test.generateLog(Status.FAIL, newtext);
    	   test.generateLog(Status.FAIL, expText);
    	   
       }
     }	

	
	@Test(groups="Manipulator")
	public void testFindwordDelete() throws Throwable{
		text = "Manjunath";
		String expText="";
		TextManipulator texmn = new TextManipulator(text);
		test = extent.createTest("FindWordDelete", "FindWordDelete")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Append Text");
       String actrepword = texmn.findWordAndDelete(text);
       String newtext = texmn.getText();
      // test.generateLog(Status.INFO, newtext);
       if (newtext.equalsIgnoreCase(expText)) {
    	   Assert.assertEquals(expText, newtext);
    	   test.log(Status.PASS, newtext);
    	   test.generateLog(Status.PASS, newtext);
    	   test.generateLog(Status.PASS, expText);
    	   
       }else {
    	   Assert.assertNotSame("Not Matched", expText, newtext);
    	   test.log(Status.FAIL, newtext);
    	   test.generateLog(Status.FAIL, newtext);
    	   test.generateLog(Status.FAIL, expText);
    	   
       }
     }	
}
