package tdd.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import javalearning.B;
import javalearning.InterviewQuestions;

public class InterviewQuestionsTest extends TestBaseClass{
	public InterviewQuestions iq = new InterviewQuestions();
	@Test(groups="Interview Questions")
	public void testIsFoo() throws Throwable{
		logger.info("Check IsFoo is true or false");
		test = extent.createTest("Validate IsFoo", "Test isFoo")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Validate isFoo");
        String param="foo";
        Boolean exps = true;
        Boolean s = iq.isFoo(param);
        String actuals = String.valueOf(s);
        if(s == exps) {
        assertTrue(s);
        test.generateLog(Status.PASS, actuals);
        }
        else if(s!=exps){
        	assertFalse(s);
        	test.generateLog(Status.PASS, actuals);
        }else {
        	test.generateLog(Status.FAIL, actuals);
        }
		
        
	}
	
	@Test(groups="Interview Questions")
	public void isDivideValueTest() throws Throwable{
		logger.info("Check IsFoo is true or false");
		test = extent.createTest("Validate IsDevide", "Test isDevide")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Validate isDevide");
		
		int a = 5;
		int b = 2;
		int expresult=2;
		int actresult = iq.isDivideValue(a, b);
		
		if(actresult==expresult) {
			assertEquals(actresult, expresult);
			test.generateLog(Status.PASS, String.valueOf(actresult));
		}else if(actresult!=expresult){
			assertNotEquals(actresult, expresult);
			test.generateLog(Status.PASS, String.valueOf(actresult));
		}else {
			test.generateLog(Status.FAIL, String.valueOf(actresult));
		}
	}
	
	@Test(groups="Interview Questions")
	public void testInheritance() throws Throwable{
		test = extent.createTest("Validate Instantiation", "Test Instantiation")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
		InterviewQuestions iq = new InterviewQuestions();
		InterviewQuestions b = new B();
		b.isFoo("foo");
	}
	
	@Test(groups="Interview Questions")
	public void testFindLargestNumber() throws Throwable{
		test = extent.createTest("Find Largest Number", "Test Largest Number")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
		  int[] numbers = new int[] {1,4,5,20,8,0};
	       int actmaxvalue = iq.findLargestNumber(numbers);
		   int expvalue = 20;
		   if(actmaxvalue==expvalue) {
		   assertEquals(actmaxvalue, expvalue);
		   test.generateLog(Status.PASS, String.valueOf(actmaxvalue));
		   }
		   else if(actmaxvalue!=expvalue) {
			assertNotEquals(actmaxvalue, expvalue);
			test.generateLog(Status.PASS, String.valueOf(actmaxvalue));
		   }else {
			   test.generateLog(Status.WARNING, String.valueOf(actmaxvalue));
		   }
	}
	
	@Test(groups="Interview Questions")
	public void testFindLargestNumberArray() throws Throwable{
		test = extent.createTest("Find Largest Number", "Test Largest Number")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
		  int[] numbers = new int[] {1,4,5,20,8,0};
	       int actmaxvalue = iq.findSecondLargestNumberInArray(numbers);
		   int expvalue = 8;
		   if(actmaxvalue==expvalue) {
		   assertEquals(actmaxvalue, expvalue);
		   test.generateLog(Status.PASS, String.valueOf(actmaxvalue));
		   }
		   else if(actmaxvalue!=expvalue) {
			assertNotEquals(actmaxvalue, expvalue);
			test.generateLog(Status.PASS, String.valueOf(actmaxvalue));
		   }else {
			   test.generateLog(Status.WARNING, String.valueOf(actmaxvalue));
		   }
	}



	
	
}
