package tdd.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.util.List;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import tdd.MissingNumber;

public class MissingNumberTestClass extends TestBaseClass{
      public MissingNumber missnum;
    @Test  
	public void MissingNubmerTest() throws Throwable{
		test = extent.createTest("Missing Number", "Missing Number")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase")
                .assignAuthor("Manjunath");
		
		int[] arr = new int[] {1,2,3,5,6};
		int actmnum = missnum.findMissingN(arr);
		int expmnum=4;
		String actmis = String.valueOf(actmnum);
		String expmis = String.valueOf(expmnum);
		if(actmnum==expmnum) {
			assertEquals(actmnum, expmnum);
			test.generateLog(Status.PASS, actmis);
			test.generateLog(Status.PASS, expmis);
		
	}else {
		assertNotEquals(actmnum, expmnum);
	}
}	
		
}