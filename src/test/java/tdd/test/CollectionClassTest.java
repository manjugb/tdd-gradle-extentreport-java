package tdd.test;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import javalearning.CollectionClass;

public class CollectionClassTest extends TestBaseClass{
	public CollectionClass cc = new CollectionClass();
	

	@Test(groups="Interview Questions")
	public void testArrayListSize() throws Throwable{
		test = extent.createTest("Find ArrayListSize", "Test ArrayListSize")
                .assignCategory("unit_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
		 int expSize=3;
		
		 int actSize=cc.ArrayListSize(2);
		 assertEquals(actSize, expSize);
		 System.out.println(actSize);
	}

	
}
