package tdd.test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import tdd.Anagram;

public class AnagramTest extends TestBaseClass{
	public Anagram agm = new Anagram();
    
	@Test(groups="Anagram")
	public void IsAnagramTest() throws Throwable{
		test = extent.createTest("Anagram or not", "this test Validate text is anagram or not")
                .assignCategory("unit_TestCase_anagram")
                .assignCategory("Postive_TestCase_anagram")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
				
		String s1 = "manju";
		String s2 = "manju";
		boolean anagram = agm.isAnagrams1(s1, s2);
	
		if(anagram) {
			assertEquals(true, anagram);
			
		}else if(!anagram){
			assertNotEquals(true, anagram);
			
		}
	}
	
	
	@Test(groups="Anagram")
	public void CheckAnagramTest() throws Throwable{
		test = extent.createTest("Anagram or not using charector arrays", "Validate text is anagram or not")
                .assignCategory("unit_TestCase_CharaArrays")
                .assignCategory("Postive_TestCase_character_arrays")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
				
		String s1 = "heart";
		String s2 = "earth";
		boolean anagram = agm.checkAnagram(s1, s2);
		String anagram1=Boolean.toString(anagram);
		if(anagram == true) {
			assertEquals(true, anagram);
			test.log(Status.PASS, anagram1);
			test.generateLog(Status.PASS, s1);
			test.generateLog(Status.PASS, s2);
		}else if(anagram == false){
			assertEquals(false, anagram);
			test.log(Status.PASS, anagram1);
			test.generateLog(Status.FAIL, s1);
			test.generateLog(Status.FAIL, s2);
		}else {
			test.log(Status.WARNING, anagram1);
		}
	}
	
	@Test(groups="Anagram")
	public void IsAnagramTest1() throws Throwable{
		test = extent.createTest("Anagram or not using charector arrays", "Validate text is anagram or not")
                .assignCategory("unit_TestCase")
                .assignCategory("Postive_TestCase_chararrays")
                .assignAuthor("Manjunath");
        logger.info("Largest Number");
				
		String s1 = "heart";
		String s2 = "earth";
		boolean anagram = agm.isAnagram2(s1, s2);
		String anagram1=Boolean.toString(anagram);
		if(anagram == true) {
			assertEquals(true, anagram);
			test.log(Status.PASS, anagram1);
			test.generateLog(Status.PASS, s1);
			test.generateLog(Status.PASS, s2);
		}else if(anagram == false){
			assertEquals(false, anagram);
			test.log(Status.PASS, anagram1);
			test.generateLog(Status.FAIL, s1);
			test.generateLog(Status.FAIL, s2);
		}else {
			test.log(Status.WARNING, anagram1);
		}
	}
	
}
